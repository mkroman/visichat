#ifndef __VISICHAT_PLAYER_HPP
#define __VISICHAT_PLAYER_HPP
#include <map>
#include <string>
#include "VisiChat/Tokenizer.hpp"

namespace VisiChat
{

class Player
{
public:
	typedef std::map<std::string, std::string> PropertiesList;

	struct Position
	{
		int x;
		int y;
	};

public:
	Player();

public:
	void parse(Tokenizer& tokenizer);
	void parseDescription(Tokenizer& tokenizer);

	void updatePosition(int x, int y);
	void setProperty(const std::string& property, const std::string& value);

	unsigned int id()
	{
		return m_id;
	}

	const std::string& name()
	{
		return m_name;
	}

	unsigned int money()
	{
		return m_money;
	}

	signed int apartment()
	{
		return m_apartment;
	}

	const struct Position& position()
	{
		return m_position;
	}

	unsigned int flags()
	{
		return m_flags;
	}

	unsigned int systemId()
	{
		return m_systemId;
	}

	const std::string& property(const std::string& property)
	{
		return m_properties[property];
	}

private:
	unsigned int    m_id;
	std::string     m_name;
	unsigned int    m_flags;
	unsigned int    m_money;
	unsigned int    m_systemId;
	signed int      m_apartment;
	PropertiesList  m_properties;
	struct Position m_position;
};

}

#endif
