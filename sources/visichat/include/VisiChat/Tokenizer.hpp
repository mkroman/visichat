#ifndef __VISICHAT_TOKENIZER_HPP
#define __VISICHAT_TOKENIZER_HPP

namespace VisiChat
{

class Tokenizer
{
public:
	/**
	 * \brief Construct a new tokenizer.
	 *
	 * @param[in] line Line to be tokenized.
	 */
	Tokenizer(const std::string& line);

	/**
	 * \brief Skip ahead \c count tokens.
	 *
	 * \throws std::out_of_range is thrown when there's less than \c count tokens ahead.
	 */
	void        skip(int count = 1);

	/**
	 * \brief Test to see if there's any tokens left.
	 *
	 * \returns If there's any tokens left, it returns true, otherwise it returns false.
	 */
	bool        hasMoreTokens();

	/**
	 * \brief Skips ahead until a numeric value is found, then casts it to an int.
	 *
	 * \throws std::out_of_range is thrown if no numeric value was found.
	 * \returns The found token casted to an int.
	 */
	int         nextInt();

	/**
	 * \brief Skips ahead until a numeric value is found, then casts it to a float.
	 *
	 * \throws std::out_of_range is thrown if no numeric value was found.
	 * \returns The found token casted to a float.
	 */
	float       nextFloat();

	/**
	 * \brief Skips to next token and returns the value as a string.
	 *
	 * \throws std::out_of_range is thrown if no tokens left.
	 * \returns The found token as a string.
	 */
	std::string nextToken();

	long nextLong();

private:
	/**
	 * \brief Test to see if every character in \c string is numeric (0-9).
	 */
	bool        stringIsNumeric(const std::string& string);

	/**
	 * \brief Test to see if the string is in a float-like format.
	 */
	bool stringIsFloat(const std::string& string);

	std::string m_line; //<! A reference to the line we're working on.
	size_t      m_needle; //<! Current end position in \c m_line.
	size_t      m_pointer; //<! Current start position in \c m_line.
};

}

#endif
