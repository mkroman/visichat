#ifndef __VISICHAT__CLIENT_HPP
#define __VISICHAT__CLIENT_HPP
#include <fcntl.h>
#include <event.h>
#include <algorithm>
#include <cstring>
#include <string>
#include <map>

#include "Console.hpp"
#include "VisiChat.hpp"
#include "VisiChat/Player.hpp"

namespace VisiChat
{

/**
 * Every connection is associated with a client, which serves as the local user.
 */
class Client
{
public:
	typedef std::vector<Player> PlayerList; //!< Player vector.
	typedef std::pair<std::string, std::string> Property; //!< Property pair type.
	typedef std::map<std::string, std::string> Properties; //!< Properties map type.

public:
	/**
	 * \brief States to acknowledge which line we just received, if the line
	 * doesn't conform to the protocol.
	 */
	enum State
	{
		WaitingForConnection,
		WaitingForStrangeBase,
		WaitingForUserLine,
		Ready
	};

public:
	/**
	 * \brief Instantiate a new Client with no credentials assigned.
	 */
	Client();

	/**
	 * \brief Instantiate a new Client with name \c user and password \c pass.
	 */
	Client(const std::string& user, const std::string& pass);

	/**
	 * \brief Destructor to release the used memory.
	 */
	~Client();

	/**
	 * \brief Create a socket to the remote server and attempt to connect to it.
	 *
	 * \returns True if connection was successfully established.
	 */
	bool establishConnection();

	void closeConnection();

	/**
	 * \brief Send the contents of \c line from the client to the server.
	 */
	void send(const std::string& line);

	/**
	 * \brief Send the contents of \c line, up to \c size bytes at most, from the client
	 * to the server.
	 */
	void send(const char* line, size_t size);

	/**
	 * \brief Send a message from the client.
	 */
	void sendMessage(const std::string& message);

	/**
	 * \brief Sets the local user id.
	 */
	void setUserId(unsigned int userId)
	{
		m_userId = userId;
	}

	/**
	 * \brief Sets a local user property.
	 */
	void setProperty(const std::string& property, const std::string& value)
	{
		m_properties[property] = value;
	}

	/**
	 * \brief Get the local user id.
	 *
	 * \returns The local user id.
	 */
	unsigned int userId() const
	{
		return m_userId;
	}

	/**
	 * \brief Get the value of a property on the local user.
	 *
	 * \returns The value as a string, the string is empty if there's no
	 * associated value.
	 */
	const std::string& property(const std::string& property)
	{
		return m_properties[property];
	}

	/**
	 * \brief Get the local users name.
	 *
	 * \returns The local users name.
	 */
	const std::string& user() const
	{
		return m_user;
	}

	/**
	 * \brief Get a player by its online id.
	 */
	Player* getPlayerById(uint id)
	{
		for (Player& player : m_players) {
			if (player.id() == id) {
				return &player;
			}
		}

		return nullptr;
	}

	/**
	 * \brief Get a player by its name.
	 */
	Player* getPlayerByName(const std::string& name, bool case_insensitive = false)
	{
		if (case_insensitive) {
			return nullptr;
		}
		else {
			for (Player& player : m_players) {
				if (player.name() == name) {
					return &player;
				}
			}
		}

		return nullptr;
	}

	/**
	 * \brief Add a new player to the list of players.
	 */
	void addPlayer(const Player& player)
	{
		m_players.push_back(player);
	}

	void removePlayer(Player& other)
	{
		m_players.erase(std::remove_if(begin(m_players), end(m_players), [&](Player& player) {
			return other.id() == player.id();
		}), end(m_players));
	}

public:
	int              fd; //!< The clients socket file descriptor.
	enum State       state; //!< The current state of the client.
	struct event*    read_event; //!< The reading event.
	struct event*    write_event; //!< The writing event.
	struct evbuffer* read_buffer; //!< The reading buffer.
	struct evbuffer* write_buffer; //!< The writing buffer.

protected:
	/**
	 * \brief Flag the file descriptor sa non-blocking.
	 */
	void setNonBlocking(int fd);

private:
	Properties   m_properties; //!< The key-value pair for client properties.
	unsigned int m_userId; //!< The local user id.
	std::string  m_user; //!< The client username.
	std::string  m_pass; //!< The client password.
	PlayerList   m_players;
};

}

#endif
