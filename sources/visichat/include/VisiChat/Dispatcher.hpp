#ifndef __VISICHAT_CONNECTIONHANDLER_HPP
#define __VISICHAT_CONNECTIONHANDLER_HPP
#include <string>

#include "VisiChat/Client.hpp"
#include "VisiChat/Controller.hpp"

namespace NetKit { namespace HTTP { class Response; } }

namespace VisiChat
{

class Client;
class Controller;

/**
 * The ConnectionHandler is used for parsing any sort of received network
 * data and then handling it appropriately.
 */
class Dispatcher
{
public:
	/**
	 * \brief Instantiate a new ConnectionHandler with an assigned controller.
	 */
	Dispatcher(Controller* controller);

	/**
	 * \brief Handle a single received line, this is the main handling entry.
	 *
	 * \param[in] line The received line, without ending newline.
	 */
	void handleLine(Client* client, const std::string& line);

	/**
	 * \brief Handle received player property and reflect the changes on associated players.
	 *
	 * \param[in] userId The id of the user of which the property was set/changed.
	 * \param[in] property The name of the property.
	 * \param[in] value The new value of the property.
	 */
	void handlePlayerProperty(Client* client, uint userId, const std::string& property, const std::string& value);

	/**
	 * \brief Forget about a player when it leaves the room.
	 */
	void handlePlayerLeaveRoom(Client* client, uint id, uint room_id, const std::string& room_name);

	/**
	 * \brief Forget about a player when it disconnects.
	 */
	void handlePlayerDisconnect(Client* client, uint id);

	/**
	 * \brief Handle received player message.
	 *
	 * \params[in] userId The id of the user sending the message.
	 * \params[in] message The users message.
	 */
	void handlePlayerMessage(Client* client, uint userId, const std::string& message);

	 /**
	  * \brief Handle received player whisper.
	  *
	  * \params[in] senderId The id of the user whispering.
	  * \params[in] recipientId The id of the user getting whispered to.
	  * \params[in] message The message being whispered.
	  */
	 void handlePlayerWhisper(Client* client, uint senderId, uint recipientId, const std::string& message);

	/**
	 * \brief Handle server command.
	 *
	 * \params[in] command The command name.
	 * \params[in] argument The command argument.
	 */
	void handleServerCommand(Client* client, const std::string& command, const std::string& argument);

	/**
	 * \brief Handle the received strangebase input and send the response.
	 *
	 * \param[in] challenge The received challenge.
	 */
	void handleStrangeBaseChallenge(Client* client, const std::string& challenge);

	/**
	 * \brief Handle the Base64 challenge - download the challenge handler, decode it
	 * and respond.
	 */
	void handleBase64Challenge(Client* client);

private:
	/**
	 * \brief Download the challenge handler file.
	 */
	NetKit::HTTP::Response* downloadChallengeHandler(std::string challengeHandler);

private:
	Controller* m_controller; //<! Pointer to the parent controller.
};

}

#endif
