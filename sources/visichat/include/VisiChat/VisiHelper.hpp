#ifndef __VISICHAT_VISIHELPER_HPP
#define __VISICHAT_VISIHELPER_HPP

#include <string>

namespace VisiChat
{

std::string encodeString(const std::string& string);
std::string encodeString(const std::wstring& string);
std::wstring decodeString(const std::string& string);

}

#endif
