#ifndef __VISICHAT_CONTROLLER_HPP
#define __VISICHAT_CONTROLLER_HPP
#include <time.h>
#include <event.h>
#include <vector>
#include <sstream>

#include "VisiChat/Dispatcher.hpp"

namespace VisiChat
{

class Client;
class Controller;
class Dispatcher;

/**
 * C++ interface that wraps member functions into a single static function,
 * when once called, calls the member function, so that they can be passed to C libraries.
 *
 * \p An example might be
 * \code
 *  Callback<Controller>* callback = new Callback<Controller>(this, &Controller::callback);
 *
 *  event = event_new(base, fd, EV_READ, Callback<Controller>::wrapper, callback);
 * \endcode
 *
 * \tparam ClassT Type of which to call an instance method on.
 */
template <typename ClassT>
class Callback
{
	/**
     * \typedef callback_type
     *
     * Functor type that libevent expects.
     */
	typedef void (ClassT::*callback_type)(int, short, Callback<Controller>*);

public:

	/**
	 * Wrap a C++ member function as a static C function.
	 *
	 * @param[in] _klass Pointer to an instance of \c ClassT.
	 * @param[in] _callback Pointer to a function with the same prototype as \c callback_type.
	 */
	Callback(ClassT* _klass, callback_type _callback, void* _data = nullptr)
		: klass(_klass), callback(_callback), data(_data)
	{}

	/**
	 * Static function that matches the prototype of libevent callbacks.
	 *
	 * Casts the \c data pointer as a callback and calls the associated \c callback function on it.
	 *
	 * @param[in] fd File descriptor associated with event.
	 * @param[in] flags Event flags.
	 * @param[in] data Data pointer that will be casted.
	 */
	static void wrapper(int fd, short flags, void* data)
	{
		Callback* pCallback = reinterpret_cast<Callback*>(data);

		(pCallback->klass->*pCallback->callback)(fd, flags, pCallback);
	}

	ClassT*       klass; //!< Pointer to instantiated instance of \c ClassT.
	callback_type callback; //!< Pointer to callback functor.
	void*         data; //!< Pointer to extra data.
};

/**
 * Self-explainatory.
 */
typedef Callback<Controller> cbController;

/**
 * Controller is encharge of all Client instances, handling data input, managing
 * the event loop and signals.
 * 
 * \note Use Controller::addClient for adding a new client, don't try to instantiate
 * your own - it will not work!
 */
class Controller
{
public:
	/**
	 * Constructor for Controller.
	 *
	 * Initializes the event base and sets the initial state.
	 */
	Controller();
	/**
	 * Destructor for Controller.
	 * Clears up the attached events, and the event base.
	 */	
	~Controller();

	/**
	 * Dispatches the event loop.
	 */
	void run();

	/**
	 * Exits the event loop and clears up any active connections.
	 */
	void close();

	/**
	 * Function that is called by \a libevent when the file descriptor triggers.
	 */
	//void callback(int fd, short flags, cbController* callback);

	void read(int fd, short flags, cbController* callback);
	void write(int fd, short flags, cbController* callback);

	/**
	 * Allocate and instantiate a new client with user and password credentials.
	 *
	 * @param user The clients username.
	 * @param pass The clients password.
	 */
	void addClient(const char* user, const char* pass);

private:
	event_base*          m_loop;
	struct event*        m_event;
	std::vector<Client*> m_clients;
	Dispatcher*          m_dispatcher;
};

}

#endif
