#ifndef __VISICHAT_BASE64_HPP
#define __VISICHAT_BASE64_HPP

#include <sstream>

namespace VisiChat
{

static const size_t Base64ArrayOffset = 0x01D7; //!< The array offset inside the java bytecode.

class Base64
{
public:
	Base64();
	Base64(const std::string& charset);

	static Base64 decodeChallengeFile(std::iostream& challengeFile);
	static inline Base64 decodeChallengeFile(const std::string& string)
	{
		std::stringstream stringStream(string);

		return decodeChallengeFile(stringStream);
	}

	const std::string& charset() const
	{
		return m_charset;
	}

	std::string decode(const std::string& input) const;
	std::string decode(const std::wstring& input) const;
	std::string encode(const std::string& input) const;
	std::string encode(const std::wstring& input) const;
	std::string decodeChallenge(const std::string& challenge);

private:
	std::string m_charset;
};

}

#endif
