#ifndef __VISICHAT_STRANGEBASE_HPP
#define __VISICHAT_STRANGEBASE_HPP

#include <string>

namespace VisiChat
{

/**
 * StrangeBase decoding/encoding class serving as a namespace.
 */
class StrangeBase
{
public:
	/**
	 * \brief Calculate the challenge key from a series of characters.
	 *
	 * \returns The computed key.
	 */
	static int         getChallengeKey(const std::string& challenge);

	/**
	 * \brief Compute the challenge response from a strangebase key.
	 *
	 * \returns The computed response.
	 */
	static std::string getChallengeResponse(int challengeKey);
};

}

#endif
