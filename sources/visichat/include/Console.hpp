#ifndef __CONSOLE_HPP
#define __CONSOLE_HPP

#include <cstdio>
#include <sstream>
#include <iostream>
#include <libgen.h>

#define FUNC_INFO //Console::Cyan << __FUNCTION__ << Console::Reset << "   "

class Console
{
public:
	enum Modifier
	{
		Black, Red, Green, Yellow, Blue, Magenta, Cyan, White,
		Reset, Bold
	};

	inline Console()
	{
	}

	inline Console(const std::string& prefix)
	{
		std::cout << escape(Bold) << " " << prefix << escape(Reset) << " ";
	}

	inline ~Console()
	{
		std::cout << escape(Reset) << std::endl;
	}

	inline Console& operator<<(const Modifier modifier)
	{
		std::cout << escape(modifier);

		return *this;
	}

	template <typename ArgT>
	inline Console& operator<<(const ArgT& value)
	{
		std::cout << value;

		return *this;
	}

	static const std::string escape(const Modifier modifier)
	{
		std::stringstream result;

		if (modifier > Black && modifier < White)
		{
			result << "\e[" << (modifier + 30) << "m";

			return result.str();
		}

		switch (modifier)
		{
			case Reset:
			{
				return std::string("\e[0m");
				break;
			}
			case Bold:
			{
				return std::string("\e[1m");
				break;
			}

			default:
			{
				return nullptr;
			}
		}

		return nullptr;
	}

private:
	std::string m_prefix;
};

class Debug : public Console
{
public:
	inline Debug() : Console("Debug")
	{

	}
};

class Error : public Console
{
public:
	inline Error() : Console("Error")
	{

	}
};

class Warn : public Console
{
public:
	inline Warn() : Console("Warn ")
	{

	}
};

#endif
