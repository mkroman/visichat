#ifndef __VISICHAT_HPP
#define __VISICHAT_HPP

#include <cstdint>
#include <iostream>

#include "VisiChat/Client.hpp"
#include "VisiChat/Base64.hpp"
#include "VisiChat/Tokenizer.hpp"
#include "VisiChat/Controller.hpp"
#include "VisiChat/VisiHelper.hpp"
#include "VisiChat/StrangeBase.hpp"

namespace VisiChat
{
static const char     kGameHost[] = "hhsrv.n.dk";
static const uint16_t kGamePort = 499;
}

#endif
