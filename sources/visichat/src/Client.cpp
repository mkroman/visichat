#include "VisiChat/Client.hpp"
#include "VisiChat/Controller.hpp"

using namespace VisiChat;

/**
 * \detail Instantiate a client with the given username and password.
 *
 * Allocate and instantiate evbuffers, set initial state to \c WaitingForStrangeBase.
 *
 * \see State
 */
Client::Client(const std::string& user, const std::string& pass)
	: m_user(user), m_pass(pass)
{
	this->state = WaitingForConnection;

	read_buffer = evbuffer_new();
	write_buffer = evbuffer_new();
}

/**
 * \detail Clean up the remaining junk.
 *
 * An important step is to delete the associated Callback instances
 * used when wrapping event C callbacks.
 */
Client::~Client()
{
	// Create a pointer to a controller calback.
	cbController* callback;

	// Get the controller callback from the read event.
	callback = reinterpret_cast<cbController*>(event_get_callback_arg(read_event));
	// Release it.
	delete callback;

	// Get the controller callback from the write event.
	callback = reinterpret_cast<cbController*>(event_get_callback_arg(write_event));
	// Release it.
	delete callback;

	// Release the events.
	event_free(read_event);
	event_free(write_event);

	// Release the buffers.
	evbuffer_free(read_buffer);
	evbuffer_free(write_buffer);
}

/**
 * \detail Create a socket, resolve the hostname, associate the
 * resolved hostname with the socket and attempt to connect.
 *
 * \todo Add per-client defined hosts for lulz.
 */
bool Client::establishConnection()
{
	int                _fd, result;
	struct hostent*    remote_host;
	struct sockaddr_in remote_addr;

	// Always zero out sin_zero!
	memset(remote_addr.sin_zero, 0, sizeof(remote_addr.sin_zero));

	// Create the socket.
	_fd = socket(AF_INET, SOCK_STREAM, 0);

	// Test to see if socket creation failed.
	if (_fd < 0) {
		Error() << "Could not create socket";

		return false;
	}

	// Resolve the hostname.
	remote_host = gethostbyname(kGameHost);

	// Test to see if resolving failed.
	if (remote_host == nullptr)
	{
		Error() << "Could not resolve remote host";

		return false;
	}

	// Set trivial attributes for our TCP connection.
	remote_addr.sin_family = AF_INET;
	remote_addr.sin_port = htons(kGamePort);

	// Copy the resolved hostname into the sockaddr structure.
	memcpy(&remote_addr.sin_addr, remote_host->h_addr, remote_host->h_length);

	// Attempt to connect.
	result = connect(_fd, reinterpret_cast<sockaddr*>(&remote_addr), sizeof(struct sockaddr));

	// Test to see if connection failed.
	if (result < 0)
	{
		Error() << "Could not connect to remote host";

		return false;
	}

	// Make sure that the socket is set to non-blocking.
	setNonBlocking(_fd);

	this->fd = _fd;
	this->state = WaitingForStrangeBase;

	// And we're done.
	return true;
}

void Client::closeConnection()
{
	if (this->state != WaitingForConnection) {
		::close(this->fd);

		event_del(this->read_event);
		event_del(this->write_event);

		this->state = WaitingForConnection;

		m_userId = 0;
		m_players.clear();
	}
}

/**
 * \detail Send the string with contents of \c line to the server.
 *
 * This actually just copies the contents to a character array and
 * sends it using Client::send(const char*, size_t).
 */
void Client::send(const std::string& line)
{
	send(line.c_str(), line.length());
}


/**
 * \detail Add the contents of \c line to the outgoing evbuffer,
 * up to \c size bytes at most.
 */
void Client::send(const char* line, size_t size)
{
	if (this->state == WaitingForConnection)
		return;

	// Append the line to the outgoing buffer.
	evbuffer_add(write_buffer, line, size);

	// Test to see if the write event is already pending.
	if (!event_pending(write_event, EV_WRITE, NULL))
	{
		// If not, add it.
		event_add(write_event, NULL);
	}
}

void Client::sendMessage(const std::string& message)
{
	std::stringstream output;

	output << " ," << m_userId << "," << encodeString(message) << "\n";

	send(output.str());
}

/**
 * \detail Get the flags of \c fd and flag it with O_NONBLOCK.
 */
void Client::setNonBlocking(int fd)
{
	int flags = fcntl(fd, F_GETFL);
	
	flags |= O_NONBLOCK;

	fcntl(fd, F_SETFL, flags);
}
