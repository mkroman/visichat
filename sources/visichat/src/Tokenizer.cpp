#include <string>
#include <iostream>
#include <stdexcept>
#include <sstream>
#include "VisiChat/Tokenizer.hpp"

using namespace VisiChat;

/**
 * \detail Instantiate a new tokenizer and set the associated string.
 */
Tokenizer::Tokenizer(const std::string& line)
	: m_line(line), m_pointer(0)
{
}

/**
 * \detail Iterate over tokens up to \c count in order to skip them.
 */
void Tokenizer::skip(int count)
{
	for (int i = 0; i < count; i++)
		nextToken();
}


/**
 * \detail Find the next instance of the delimiting character and return the
 * following data up to the next delimiting character, or to the end if no
 * delimiting character is available.
 */
std::string Tokenizer::nextToken()
{
	size_t      needle;
	std::string result;

	needle = m_line.find(',', m_pointer);

	if (needle == std::string::npos) {
		if (m_pointer < m_line.length()) {
			result = m_line.substr(m_pointer, m_line.length() - m_pointer);

			m_pointer = m_line.length();
		}
		else {
			throw std::out_of_range("Reached end of line");
		}
	}
	else {
		result = m_line.substr(m_pointer, needle - m_pointer);

		m_pointer = needle + 1;
	}

	return result;
}

/**
 * \detail Iterate over every token until a numeric value is found, which
 * is then casted to an int.
 */
int Tokenizer::nextInt()
{
	int result = -1;
	std::string value;

	while (hasMoreTokens())
	{
		value = nextToken();

		if (stringIsNumeric(value)) {
			std::stringstream stringStream(value);
			stringStream >> result;

			return result;
		}
	}

	return result;
}

/**
 * \detail Iterate over every token until a numeric value is found, which
 * is then casted to a float.
 */
float Tokenizer::nextFloat()
{
	float result = -1;
	std::string value;

	while (hasMoreTokens())
	{
		value = nextToken();

		if (stringIsFloat(value)) {
			std::stringstream stringStream(value);
			stringStream >> result;

			return result;
		}
	}

	return result;
}

long Tokenizer::nextLong()
{
	long result = -1;
	std::string value;

	while (hasMoreTokens())
	{
		value = nextToken();

		if (stringIsNumeric(value)) {
			std::stringstream stringStream(value);

			stringStream >> result;

			return result;
		}
	}

	return result;
}

/**
 * \detail Test to see if the input string contains nothing but numbers.
 *
 */
bool Tokenizer::stringIsNumeric(const std::string& string)
{
	for (char character : string)
	{
		if ((character < '0' || character > '9') && character != '-')
			return false;
	}

	return true;
}

/**
 * \detail Test to see if the input string contains numbers and period marks.
 */
bool Tokenizer::stringIsFloat(const std::string& string)
{
	for (char character : string)
	{
		if ((character < 48 || character > 57) && character != 46)
			return false;
	}

	return true;
}

/**
 * \detail Test to see if there's any tokens left by searching for the
 * next delimiter.
 */
bool Tokenizer::hasMoreTokens()
{
	size_t next = m_line.find(',', m_pointer);

	if (next != std::string::npos || m_pointer < m_line.length())
		return true;
	else
		return false;
}
