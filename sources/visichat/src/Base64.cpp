#include <string>
#include <cstring>
#include <istream>

#include "Console.hpp"
#include "VisiChat.hpp"
#include "VisiChat/Base64.hpp"

using namespace VisiChat;

Base64::Base64()
	: m_charset("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/")
{
}

Base64::Base64(const std::string& charset)
{
	int i, ciphers[64];

	for (i = 0; i < 64; i++)
	{
		ciphers[i] = i + 20;
	}

	for (i = 0; i < 64; i++)
	{
		m_charset.push_back(ciphers[static_cast<int>(charset[i])]);
	}
}

std::string Base64::decode(const std::wstring& input) const
{
	std::string octetInput;

	octetInput.assign(begin(input), end(input));

	return decode(octetInput);
}

std::string Base64::decodeChallenge(const std::string& inputChallenge)
{
	std::wstring challenge = decodeString(inputChallenge);

	return decode(challenge);
}

std::string Base64::decode(const std::string& input) const
{
	int i = 0, j = 0;
	unsigned char left[3];
	unsigned char right[4];
	std::string result;

	int position, length, pointer = 0;

	length = input.length();
	position = length;

	while (position--)
	{
		right[i++] = input[pointer]; pointer++;

		if ((pointer == length - 1 || pointer == length - 2) && input[pointer] == '=')
		{
			break;
		}

		if (i == 4)
		{
			for (i = 0; i < 4; i++)
			{
				right[i] = m_charset.find(right[i]);
			}

			left[0] = (right[0] << 2) + ((right[1] & 0x30) >> 4);
			left[1] = ((right[1] & 0xf) << 4) + ((right[2] & 0x3c) >> 2);
			left[2] = ((right[2] & 0x3) << 6) + right[3];

			for (i = 0; i < 3; i++)
			{
				result += left[i];
			}

			i = 0;
		}
	}

	if (i)
	{
		for (j = i; j < 4; j++)
		{
			right[j] = 0;
		}

		for (j = 0; j < 4; j++)
		{
      		right[j] = m_charset.find(right[j]);
      	}

		left[0] = (right[0] << 2) + ((right[1] & 0x30) >> 4);
		left[1] = ((right[1] & 0xf) << 4) + ((right[2] & 0x3c) >> 2);
		left[2] = ((right[2] & 0x3) << 6) + right[3];

		for (j = 0; (j < i - 1); j++)
		{
			result += left[j];
		}
	}

	return result;
}

std::string Base64::encode(const std::wstring& input) const
{
	std::string octetInput;
	
	octetInput.assign(begin(input), end(input));

	return encode(octetInput);
}

std::string Base64::encode(const std::string& input) const
{
	int i = 0, j = 0;
	int length;
	char *pointer;
	unsigned char left[3];
	unsigned char right[4];
	std::string result;

	length = input.length();
	pointer = const_cast<char*>(input.c_str());

	while (length--)
	{
		left[i++] = *(pointer++);

		if (i == 3)
		{
			right[0] = (left[0] & 0xfc) >> 2;
			right[1] = ((left[0] & 0x03) << 4) + ((left[1] & 0xf0) >> 4);
			right[2] = ((left[1] & 0x0f) << 2) + ((left[2] & 0xc0) >> 6);
			right[3] = left[2] & 0x3f;

			for(i = 0; i < 4; i++)
			{
				result += m_charset[right[i]];
			}

			i = 0;
		}
	}

	if (i != 0)
	{
		for (j = i; j < 3; j++)
		{
			left[j] = '\0';
		}

		right[0] = (left[0] & 0xfc) >> 2;
		right[1] = ((left[0] & 0x03) << 4) + ((left[1] & 0xf0) >> 4);
		right[2] = ((left[1] & 0x0f) << 2) + ((left[2] & 0xc0) >> 6);
		right[3] = left[2] & 0x3f;

		for (j = 0; j < i + 1; j++)
		{
			result += m_charset[right[j]];
		}

		while (i++ < 3)
		{
			result += '=';
		}
	}

	return result;
}

Base64 Base64::decodeChallengeFile(std::iostream& challengeFile)
{
	char byte;
	std::string charset;

	if (challengeFile.good())
	{
		challengeFile.seekg(Base64ArrayOffset);

		for (int i = 0; i < 64; i++)
		{
			// Read the first instruction, it should be aload_2.
			challengeFile.get(byte);

			if (byte != 0x2C)
			{
				Warn() << "Did not get the expected aload_2 instruction!";
			}

			// Read the next instruction, it should be either iconst_x or bipush.
			challengeFile.get(byte);

			if (byte == 0x10)
			{ // bipush
				challengeFile.get(); // skip the index
			}
			else if (byte >= 0x03 && byte <= 0x08) // iconst_x
			{
				// do nothing
			}
			else {
				Warn() << "Did not get the expected bipush/iconst_x instruction!";
			}

			// Read the next instruction, it should be either iconst_x or bipush.
			challengeFile.get(byte);

			if (byte == 0x10) // bipush
			{
				challengeFile.get(byte);

				charset.push_back(byte);
			}
			else if (byte >= 0x03 && byte <= 0x08) // iconst_x
			{
				charset.push_back(byte - 0x03);
			}
			else
			{
				Warn() << "Did not get the expected bipush/iconst_x instruction!";
			}

			// Read the last instruction, it should be iastore.
			challengeFile.get(byte);

			if (byte != 0x4F)
			{
				Warn() << "Did not get the expected iastore instruction!";
			}
		}
	}
	else
	{
		Error() << "Could not open challenge file!";
	}

	return Base64(charset);
}
