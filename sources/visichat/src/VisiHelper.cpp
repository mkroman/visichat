#include <string>
#include <sstream>
#include <iostream>

#include "VisiChat/VisiHelper.hpp"

std::string VisiChat::encodeString(const std::string& string)
{
	int i;
	char out;
	std::stringstream result;

	for (char in : string)
	{
		if (in < 32 || in > 127 || in == 42 || in == 38 || in == 44 || in == 39)
		{
			result << '&';

			for (i = 0; i < 4; i++)
			{
				out = (in & 0xF000) >> 12;
				in <<= 4;

				result << static_cast<char>(out + 65);
			}
		}
		else
		{
			result << (char)in;
		}
	}

	return result.str();
}

std::string VisiChat::encodeString(const std::wstring& string)
{
	int i;
	wchar_t out;
	std::stringstream result;

	for (wchar_t in : string)
	{
		if (in < 32 || in > 127 || in == 42 || in == 38 || in == 44 || in == 39)
		{
			result << '&';

			for (i = 0; i < 4; i++)
			{
				out = (in & 0xF000) >> 12;
				in <<= 4;

				result << static_cast<char>(out + 65);
			}
		}
		else
		{
			result << (char)in;
		}
	}

	return result.str();
}

std::wstring VisiChat::decodeString(const std::string& string)
{
	char in;
	int i, j, m, length;
	std::wstringstream result;

	length = static_cast<int>(string.length());

	for (i = 0; i < length; i++)
	{
		in = string[i];

		if (in == '&')
		{
			if (length < i + 5)
				break;

			m = 0;

			for (j = 0; j < 4; j++)
			{
				m *= 16;
				m += string[++i] - 'A';
			}

			result << static_cast<wchar_t>(m);
		}
		else
		{
			result << static_cast<wchar_t>(in);
		}
	}

	return result.str();
}
