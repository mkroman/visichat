#include "Console.hpp"
#include "netkit/http.hpp"
#include "VisiChat/Tokenizer.hpp"
#include "VisiChat/Dispatcher.hpp"
#include "VisiChat/StrangeBase.hpp"

using namespace NetKit;
using namespace VisiChat;

/**
 * \detail Set \c m_controller to the pointer \c controller.
 */
Dispatcher::Dispatcher(Controller* controller)
	: m_controller(controller)
{
}

/**
 * \detail Parse the received line and take care of it, if possible.
 */
void Dispatcher::handleLine(Client* client, const std::string& line)
{
	std::cout << "<< " << line << std::endl;

	if (client->state == Client::Ready)
	{
		Tokenizer tokenizer(line);

		try {
			switch (line[0])
			{
				case 'C':
				{
					int userId;
					std::string property, value;

					userId = tokenizer.nextInt();

					tokenizer.skip();

					property = tokenizer.nextToken();

					if (tokenizer.hasMoreTokens())
					{
						value = tokenizer.nextToken();
					}

					handlePlayerProperty(client, userId, property, value);

					break;
				}

				case ' ':
				{
					int userId;
					std::string message;

					userId = tokenizer.nextInt();
					message = tokenizer.nextToken();

					handlePlayerMessage(client, userId, message);

					break;
				}

				case '%':
				{
					int senderId, recipientId;
					std::string message;

					senderId = tokenizer.nextInt();
					recipientId = tokenizer.nextInt();
					message = tokenizer.nextToken();

					handlePlayerWhisper(client, senderId, recipientId, message);

					break;
				}

				case '3':
				{
					std::string command, argument;

					tokenizer.skip(); // Skip '3'.
					command = tokenizer.nextToken();
					argument = tokenizer.nextToken();

					handleServerCommand(client, command, argument);

					break;
				}

				case 'o':
				{
					Player player;

					// Skip leading token.
					tokenizer.skip();

					// Parse the rest of the line.
					player.parse(tokenizer);

					std::cout << "player entered the room: " << player.name() << std::endl;

					client->addPlayer(player);

					break;
				}

				case 'y':
				{
					size_t index = 0, last_index = 2;

					// Find the first occurence.
					index = line.find(",,", 2);

					while (index != std::string::npos) {
						// Cut the string.
						std::string info = line.substr(last_index, index - last_index);

						// Create a tokenizer for the info and parse it as a player instance.
						Player player;
						Tokenizer info_tokenizer(info);

						// Parse it.
						player.parse(info_tokenizer);

						// Add it to the client.
						client->addPlayer(player);

						// Find the next occurence.
						last_index = index + 2;
						index = line.find(",,", last_index);
					}

					break;
				}

				case 'v':
				{
					uint id = 0;
					uint new_room = 0;
					std::string room_name;

					// Skip the leading token.
					tokenizer.skip();

					// Get the user id.
					id = tokenizer.nextInt();

					// Get the new room id.
					new_room = tokenizer.nextInt();

					// Get the new room's name.
					room_name = tokenizer.nextToken();

					handlePlayerLeaveRoom(client, id, new_room, room_name);

					break;
				}

				case 'x':
				{
					uint id;

					// Skip the leading token.
					tokenizer.skip();

					// Get the user id.
					id = tokenizer.nextInt();

					handlePlayerDisconnect(client, id);
				}

				default:
				{

					// Not implemented yet.
				}
			}
		} catch(std::exception& e)
		{
			std::cout << std::endl;
			std::cout << "\e[31mError!\e[0m" << std::endl;
			std::cout << "\e[1mMessage:\e[0m " << e.what() << std::endl;
			std::cout << "\e[1mWhen handling line:\e[0m " << line << std::endl << std::endl;
		}
	}
	else if (client->state == Client::WaitingForStrangeBase)
	{
		handleStrangeBaseChallenge(client, line);

		client->state = Client::WaitingForUserLine;
	}
	else if (client->state == Client::WaitingForUserLine)
	{
		wchar_t w = line[0];
		std::wcout << (int)w << std::endl;

		// Where to store our user id.
		int userId;
		// Instantiate a tokenizer.
		Tokenizer tokenizer(line);

		// Get the user id - it should be the first token.
		userId = tokenizer.nextInt();

		// Set our user id.
		client->setUserId(userId);

		client->state = Client::Ready;
	}
}

/**
 * \detail Compute and send the strangebase response.
 */
void Dispatcher::handleStrangeBaseChallenge(Client* client, const std::string& challenge)
{
	int challengeKey;
	std::string challengeResponse;

	challengeKey = StrangeBase::getChallengeKey(challenge);
	challengeResponse = StrangeBase::getChallengeResponse(challengeKey * challengeKey);

	std::stringstream output;

	output << challengeResponse << ",-1," << "adf6d697-1891-4c02-8017-0920ec9024df" << (char)0;

	client->send(output.str());
}

/**
 * \detail Update the local client properties if the server updated our
 * player.
 */
void Dispatcher::handlePlayerProperty(Client* client, uint userId, const std::string& property, const std::string& value)
{
	// Test to see if this is our client.
	if (userId == client->userId())
	{
		client->setProperty(property, value);
	}
	else {
		Player* player = client->getPlayerById(userId);

		player->setProperty(property, value);
	}
}

/**
 * \detail Don't actually do anything functional yet.
 */
void Dispatcher::handlePlayerMessage(Client* client, uint id, const std::string& message)
{
	Player* player = client->getPlayerById(id);
}

/**
 * \detail Don't actually do anything functional yet.
 */
 void Dispatcher::handlePlayerWhisper(Client* client, uint senderId, uint recipientId, const std::string& message)
 {
 	std::cout << "[" << senderId << " -> " << recipientId << "] whispers: " << message << std::endl;
 }

 /**
  * \detail Check if the server wants us to load the challenge handler.
  */
void Dispatcher::handleServerCommand(Client* client, const std::string& command, const std::string& argument)
{
	if (command == "load")
	{
		// Handle the base64 challenge.
		if (argument == "n7.security.client.ChallengeHandler")
		{
			handleBase64Challenge(client);

			// Let's sneak this in there aswell.
			std::stringstream output;

			output << "T,-1," << client->userId() * 3 << "\n";

			client->send(output.str());
		}

		std::cout << "Note: server wanted us to load a dynamic thing: " << argument << std::endl;
	}
	else if (command == "hour")
	{
		size_t offset = argument.find(':');
		std::string sum, hour;

		hour = argument.substr(0, offset);
		sum = argument.substr(offset + 1);

		std::cout << "Received hour " << hour << "!" << std::endl;

		std::stringstream output;

		output << "3,time," << sum << "\n";

		client->send(output.str());
	}
	else
	{
		std::cout << "Note: unhandled server command received: " << command << "," << argument << std::endl;
	}
}

void Dispatcher::handlePlayerLeaveRoom(Client* client, uint id, uint room_id, const std::string& room_name)
{
	Player *player = client->getPlayerById(id);

	if (player != nullptr) {
		std::cout << "Player [" << player->id() << "] " << player->name() << " left the room." << std::endl;

		client->removePlayer(*player);
	}
	else {

	}
}

void Dispatcher::handlePlayerDisconnect(Client* client, uint id)
{
	Player* player = client->getPlayerById(id);

	if (player != nullptr) {
		std::cout << "Player [" << player->id() << "] " << player->name() << " disconnected." << std::endl;
     
		client->removePlayer(*player);
	}
	else {

	}
}

/**
 * \detail Downloads the challenge handler class and decodes it, then it takes
 * the decoded java bytecode array and instantiates a Base64 instance that we
 * can use for decrypting the base64 challenge.
 *
 * If the challenge handler was successfully downloaded, send the challenge
 * response.
 */
void Dispatcher::handleBase64Challenge(Client* client)
{
	std::string challengeString = client->property("_challengeString");
	std::string challengeHandlerString = client->property("_challengeHandlerString");

	Debug() << Console::Yellow << "Downloading Base64 challenge handler";

	HTTP::Response* response = downloadChallengeHandler(challengeHandlerString);

	if (response->code() == 200)
	{
		Debug() << Console::Yellow << "Download was successful";

		Base64 encoder = Base64::decodeChallengeFile(response->body());
		std::stringstream output;

		output << "3,ChallengeHandlerResponse," << encoder.decodeChallenge(challengeString) << "\r\n";

		client->send(output.str());
	}

	delete response;
}

/**
 * \detail Make the challenge handler property into a url and download the challenge
 * handler.
 */
HTTP::Response* Dispatcher::downloadChallengeHandler(std::string challengeHandler)
{
	std::replace(begin(challengeHandler), end(challengeHandler), '.', '/');
	challengeHandler.insert(0, "/chat/");
	challengeHandler.append(".class");

	HTTP::Client client("hhsrv.n.dk", 80);
	HTTP::Request request(challengeHandler);
	HTTP::Response* response;

	response = client.get(request);

	return response;
}
