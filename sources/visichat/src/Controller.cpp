#include <fcntl.h>
#include <cstring>
#include <sstream>
#include <iostream>
#include <algorithm>

#include "netkit.hpp"
#include "Console.hpp"
#include "VisiChat.hpp"

using namespace NetKit;
using namespace VisiChat;

Controller::Controller()
	: m_dispatcher(new Dispatcher(this))
{
	m_loop = event_base_new();
}

Controller::~Controller()
{
	// Release client instances.
	for (auto client : m_clients)
	{
		delete client;
	}

	// Release the event loop.
	event_base_free(m_loop);

	// Release the connection handler.
	delete m_dispatcher;
}

void Controller::run()
{
	// Dispatch the event loop.
	event_base_dispatch(m_loop);
}

void Controller::close()
{
	// Close the event loop.
	event_base_loopexit(m_loop, NULL);
}

void Controller::write(int fd, short flags, cbController* callback)
{
	Client* client = reinterpret_cast<Client*>(callback->data);

	int written = 0;
	size_t size = 0;
	size = evbuffer_get_length(client->write_buffer);

	if (size > 0)
	{
		written = evbuffer_write(client->write_buffer, client->fd);

		if (written < (int)size)
		{
			event_add(client->write_event, NULL);
		}
	}
}

void Controller::read(int fd, short flags, cbController* callback)
{
	Client* client = reinterpret_cast<Client*>(callback->data);

	char* line;
	int result = 0;
	size_t size;

	result = evbuffer_read(client->read_buffer, client->fd, 512);

	if (result == 0)
	{
		std::cout << "Connection closed" << std::endl;

		return;
	}
	else if (result == -1)
	{
		std::cout << "Socket error" << std::endl;

		return;
	}

	line = evbuffer_readln(client->read_buffer, &size, EVBUFFER_EOL_LF);

	while (line != NULL)
	{
		m_dispatcher->handleLine(client, std::string(line, size));

		free(line);
		
		line = evbuffer_readln(client->read_buffer, &size, EVBUFFER_EOL_LF);
	}

	event_add(client->read_event, NULL);
}

void Controller::addClient(const char* user, const char* pass)
{
	Client* client;
	Callback<Controller>* read_callback;
	Callback<Controller>* write_callback;

	// Allocate and instantiate a new client.
	client = new Client(user, pass);
	read_callback = new Callback<Controller>(this, &Controller::read, client);
	write_callback = new Callback<Controller>(this, &Controller::write, client);

	// Create a new socket and attach the file descriptor to the client.
	if (client->establishConnection())
	{
		// Create new i/o events.
		client->read_event = event_new(m_loop,
									   client->fd,
									   EV_READ,
									   Callback<Controller>::wrapper,
									   read_callback);

		client->write_event = event_new(m_loop,
									    client->fd,
									    EV_WRITE,
									    Callback<Controller>::wrapper,
									    write_callback);

		// Add the events.
		event_add(client->read_event, NULL);
		event_add(client->write_event, NULL);

		m_clients.push_back(client);
	}
	else
	{
		Error() << "Could not attach socket to client!";

		delete client;
		delete read_callback;
		delete write_callback;
	}
}
