#include "VisiChat.hpp"
#include "VisiChat/StrangeBase.hpp"

using namespace VisiChat;

int StrangeBase::getChallengeKey(const std::string& challenge)
{
	int i = 0, length = 0;

	length = static_cast<int>(challenge.length());

	for (int j = 0; j < length; j++)
		i = ((i * 17) + (challenge[j] - 97));

	return i & 0x3fff;
}

std::string StrangeBase::getChallengeResponse(int challengeKey)
{
	int i, value;
	std::string buffer;

	for (i = 13; i < challengeKey; i *= 13)
		;

	for (value = 0; i > 0; i /= 13)
	{
		value = challengeKey / i;
		buffer.push_back(97 + value);
		challengeKey -= i * value;
	}

	return buffer;
}
