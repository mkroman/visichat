#include <iostream>
#include "VisiChat/Player.hpp"

using namespace VisiChat;

Player::Player()
{
}

void Player::parse(Tokenizer& tokenizer)
{
	// Get the user id.
	m_id = tokenizer.nextInt();

	// Skip the user class.
	tokenizer.skip();

	// Get the user name.
	m_name = tokenizer.nextToken();

	// Get the system id.
	m_systemId = tokenizer.nextInt();

	// Get the user flags.
	m_flags = tokenizer.nextInt();

	// Count the users money.
	m_money = tokenizer.nextInt();

	// Get the users apartment number (-1 if none).
	m_apartment = tokenizer.nextInt();

	parseDescription(tokenizer);
}

void Player::parseDescription(Tokenizer& tokenizer)
{
	// Get the users location (x, y).
	m_position.x = tokenizer.nextInt();
	m_position.y = tokenizer.nextInt();

	tokenizer.skip(6);

	// Get the parameters count.
	if (tokenizer.hasMoreTokens()) {
		std::string name, value;
		int parameters = tokenizer.nextInt();

		// Read each parameter.
		for (int i = 0; i < parameters; i++)
		{
			name = tokenizer.nextToken();
			value = tokenizer.nextToken();

			// // Save the parameter set.
			m_properties[name] = value;
		}

		if (tokenizer.hasMoreTokens()) {
			// std::cout << std::endl  << std::endl << std::endl << "Tokenizer isn't empty!!!" << std::endl;
			// std::cout << tokenizer.nextToken() << std::endl << std::endl;
		}
	}
}

void Player::updatePosition(int x, int y)
{
	m_position.x = x;
	m_position.y = y;
}

void Player::setProperty(const std::string& property, const std::string& value)
{
	m_properties[property] = value;
}
