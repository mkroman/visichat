#include <cstdio>

#include "VisiChat.hpp"

// Prototypes
void printHelp(const char* program);

static VisiChat::Controller controller;

// Implementations
int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		printHelp(argv[0]);

		return 1;
	}

	controller.addClient(argv[1], argv[2]);

	controller.run();
	return 0;
}

void printHelp(const char* program)
{
	printf("VisiChat 0.1.0\r\n");
	printf("Usage: %s [user] [pass]\r\n\r\n", program);

	printf("  -h, --help\tprint help dialog and exit\r\n\r\n");
}
